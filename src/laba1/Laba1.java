/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laba1;

import java.io.*;
/**
 *
 * @author Artem
 */
public class Laba1 {
    public static void main(String[] args) {
        try
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Введите степень при е: ");
            String enter = in.readLine();
            int stepen = Integer.parseInt (enter); 
            myThread t;
            t = new myThread(stepen);
            Thread t1 = new Thread(t);
            t1.start ();
            
            while(true)
            {
                String enter1 = in.readLine();
                if (enter1.equals ("quit"))
                {
                    // Остановка потока
                    t1.interrupt ();
                    t1.join ();
                    return;
                }
                System.out.println ("Текущий результат: " + t.getResult ());
            }
        }
        catch(Exception e)
        {
            e.printStackTrace ();
        }
    }
}
class myThread implements Runnable{
        private double result; // текущий результат
        private int stepen; // степень при е
        // Конструктор потока
        public myThread(int stepen)
        {
            this.stepen = stepen;
            this.result = 0;
        }
        // Метод, который вызывается при старте потока
        public void run()
        {
            try
            {
                exp(0);
            }
            catch(InterruptedException e)
            {
                return;
            }
        }
        private void exp(int count) throws InterruptedException
        {
            // Прибавим следующий член ряда
            result += Math.pow (stepen, count) / factorial(count);
            Thread.sleep (1000);
            exp(++count);
        }
        // Вычисление факториала
        private int factorial(int number)
        {
            if (number == 0)
                number = 1;
            else
                number *= factorial(--number);

            return number;
        }
        public synchronized double getResult()
        {
            return result;
        }
 }
